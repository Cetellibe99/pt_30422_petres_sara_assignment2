This project is a queue simulator of a shop. <br>
The main task was to work with Java threads. <br>
- The program simulates the arrival and the service process for each client. <br>
- The distribution of the clients to the queues are not random, each client goes to the queue where the smallest number of clients are waiting <br>
- The output is the status of the queues in each unit of time until simulation ends. <br>
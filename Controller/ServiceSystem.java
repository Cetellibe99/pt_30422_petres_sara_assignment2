package Controller;
import Model.Qu;
import Model.Client;
import Model.ClientCopy;
import Model.WaitingQueue;
import java.util.*;
import java.io.*;

/** @author Petres Sara 30422
 * @since 27/3/2020
 *
 * The ServiceSystem class describes the mechanism of this system
 *
 * It has three significant methods:
 *
 * I. The run() method which describes the behavior of the shop, i.e.
 * invokes the run() methods of the WaitingQueue and the Qu (queue) classes
 * and estabilish the orders of the used methods to run the shop correctly
 *
 * II. The decision() method is responsible for arranging the clients in queues when they
 * "entered" the shop in such a way that there is no any pair of queues where
 * q1.size()>=q2.size()+2
 *
 * III. The outText() and printEveryone() methods are responsible for
 * displaying the output. The outText() writes in the Out-Test.txt
 * and the printEveryone() displays the output in the console
 *
 * */
public class ServiceSystem extends Thread {

    public List <Qu> queues;
    public WaitingQueue wq;
    public List <Client> clients;
    public int time;
    public String outstring;
    public ServiceSystem(WaitingQueue wq, int time) {
        this.wq = wq;
        this.time = time;
        queues= new ArrayList<>();
        clients = new ArrayList<>();
    }
    /** Initialize the queues*/
    public void setQueuesClosed() {
        for(Qu q : queues) {
            q.status = "closed";
        }
    }
    public void setQ(Qu q) {
        this.queues.add(q);
    }
    public void setTime(int time) {
        this.time = time;
    }

    /** clientCopy and client are 2 very similar datastructure
     * SEE the description and the usage of ClientCopy in the class ClientCopy*/

    public Client findCorrespondingClient(ClientCopy cc) {
        Client c=null;
        for(Client client : clients) {
            if(client.ID == cc.ID)
                c = client;
        }
        return c;
    }

    public void decision(List <ClientCopy> removedList) {

        /**removedList conatins those clients which arrived to the shop
         * but aren't aranged yet in the queues*/

        while (removedList.size()>0) {
            /** place one element in each queue*/
            for (int i=0; i<queues.size(); ++i) {
                if (queues.get(i).quCopy.isEmpty() && removedList.size()>0)
                {
                    queues.get(i).quCopy.add(removedList.get(removedList.size()-1));
                    removedList.remove(removedList.size()-1);
                }
            }
            /** IF there are more elements to arrange in the queues => */
            if(removedList.size()==0)
                break;
            else {

                /**@param ind: set 1 elements in each queue, if there are more in
                 * waiting queue, set 2 in each queue etc.
                 * what matters that each queue's length should be equal or +-1*/

                for (int ind = 1; removedList.size()>0; ++ind) {
                    for (Qu queue: queues) {
                        if(queue.quCopy.size()==ind && removedList.size()>0) {
                            queue.quCopy.add(removedList.get(removedList.size()-1));
                            removedList.remove(removedList.size()-1);
                        }
                    }
                }
            }

        }
    }

    /**isTerminated() checks if the run() method finished its job: if the queues and the waitingqueue are empty*/
    Boolean isTerminated() {
        Boolean simulationTerminated = true;
        if (wq.waitingClients.isEmpty()==false)
            simulationTerminated = false;
        else  {

            for(Qu q : queues) {
                if (q.status!="closed")
                    simulationTerminated = false;
            }
        }
        return simulationTerminated;
    }

    /** writes in the output text file the output text in the required format**/
    public void outText(FileWriter myWriter, int printTime) {

        try {
            myWriter.write("\n \n Time " + printTime);
            myWriter.write("\n Waiting clients: ");
            Client c= new Client(0,0,0);
            Set set = wq.waitingClients.entrySet();
            Iterator i = set.iterator();
            while(i.hasNext()) {
                Map.Entry me = (Map.Entry)i.next();
                ClientCopy cc = (ClientCopy) me.getKey();
                c = findCorrespondingClient(cc);
                myWriter.write ("("+c.ID+","+c.arrivalTime+ ","+c.serviceTime+")  ");
            }

            myWriter.write("\n");
            for (int j=0; j<queues.size(); ++j) {
                myWriter.write("\n Queue "+j+":  ");

                if (queues.get(j).quCopy.size()==0) {
                    queues.get(j).status="closed";
                    myWriter.write(queues.get(j).status);
                }
                else {
                    queues.get(j).status = "open";

                    try {
                        Iterator <ClientCopy> itr = queues.get(j).quCopy.iterator();

                        while(itr.hasNext()) {
                            c = findCorrespondingClient(itr.next());
                            myWriter.write ("("+c.ID+","+c.arrivalTime+ ","+c.serviceTime+")  ");
                        }
                    }
                    catch(ConcurrentModificationException e) {
                        System.out.println(e);
                    }
                }
            }

        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
    /** shows the output in the console*/
    public void printEveryone(int printTime) {

        System.out.println("");
        System.out.println("Time "+printTime);
        System.out.print("Waiting clients: ");
        Client c = new Client(0,0,0);
        Set set = wq.waitingClients.entrySet();
        Iterator i = set.iterator();
        while(i.hasNext()) {
            Map.Entry me = (Map.Entry)i.next();
            ClientCopy cc = (ClientCopy) me.getKey();
            c = findCorrespondingClient(cc);
            c.printClientOut();
            System.out.print(" ");
        }

        System.out.println("");
        for (int j=0; j<queues.size(); ++j) {
            System.out.println("");
            System.out.print("Queue " +j+": ");

            if (queues.get(j).quCopy.size()==0) {
                queues.get(j).status="closed";
                System.out.print(queues.get(j).status);
            }
            else {
                queues.get(j).status = "open";
                try {
                    Iterator <ClientCopy> itr = queues.get(j).quCopy.iterator();

                    while(itr.hasNext()) {
                        c = findCorrespondingClient(itr.next());
                        c.printClientOut();
                    }
                }
                catch(ConcurrentModificationException e) {
                    System.out.println(e);
                }
            }
        }
    }

    public void run() {

        /** define output file, and print the output sequentially (with 1000 milis stepsize)*/

        try {

          // FileWriter myWriter = new FileWriter("Out-Test.txt");
           FileWriter myWriter = new FileWriter(outstring);

            int avgWaitingTime = 0;

            for (int i=0; i<time && (!isTerminated());++i) { //TIME

                /** each cycle of this loop describes action in one second of the simulation*/

                try {
                    System.out.println(" ");
                    /** firstly the waiting queue's thread does its job*/
                    wq.run();
                    wq.interrupt();

                    /** then if clients arrived to the shop(then they are put on
                     *  the waiting queues removedList) decide who to
                     * which queue should go */
                    decision(wq.removedList);

                    Thread.sleep(1000);

                    printEveryone(i);
                    outText(myWriter,i);

                    /** Also invoke the run() method of the queues, which decides
                     * if someone finished the shopping and should leave the shop
                     * Moreover, to increase the waiting time of the clients in the shop*/
                    for (int j=0; j<2; ++j) {
                        queues.get(j).run();
                    }
                    for (int j=0; j<2; ++j) {
                        queues.get(j).interrupt();
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            /**COUNT AVERAGE WAITING TIME : the total time spend by each
             *  client at the queue divided by the nr of clients*/

            /** the waiting time of those who remained in the queues after time out*/
            for (Qu q : queues) {
                try {
                    Iterator <ClientCopy> itr = q.quCopy.iterator();

                    while(itr.hasNext()) {
                        avgWaitingTime += itr.next().waitingTime;
                    }
                }
                catch(ConcurrentModificationException e) {
                    System.out.println(e);
                }
            }

            /** the time of those who left the queues, finsihed shopping
             * it was measured in the run() method of the queues when a client
             * was removed form the queue*/
            for (Qu q : queues) {
                avgWaitingTime += q.wTime;
            }

            System.out.println("");
            Double nrofclients = (double) clients.size();
            System.out.println("\n Average waiting time : "+avgWaitingTime/nrofclients);


          //  myWriter.write("\n \n Average waiting time : "+avgWaitingTime/nrofclients);

            /** close output file*/
            myWriter.close();
        } catch (Exception e1) {

            e1.printStackTrace();
        }







    }




}

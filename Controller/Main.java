package Controller;


import Model.Client;
import Controller.ServiceSystem;
import Model.Qu;
import Model.WaitingQueue;
import java.util.Random;
import java.util.Scanner;
import java.io.*;
import Model.ClientCopy;

/**
 * @author Petres Sara 30422
 * @version 1.0
 * @since 3/27/2020
 *
 * This program simulates a queuing based system for a shop
 * The aim is to minimalize the client's waiting time and provide an efficient
 * implementation using threads
 *
 *The Main class represents:
 *reading the data input parameters from file
 *the generation of the random input data by the given parameters
 *invokes the run() method of the ServiceSystem class which is the "brain" of this program
 */


public class Main {

    /**Random Client Generator method*/

    public static Client [] generateClients(int clientnr, int minArrivalTime, int maxArrivalTime, int minServiceTime, int maxServiceTime) {

        Client [] clients = new Client[clientnr];
        Random rand = new Random();
        int arrTime, servTime;
        for(int i=0; i<clientnr; ++i) {
            arrTime = rand.nextInt(maxArrivalTime - minArrivalTime+1) + minArrivalTime;
            servTime =rand.nextInt(maxServiceTime - minServiceTime+1) + minServiceTime;
            clients[i] = new Client(i+1,arrTime,servTime);
        }

        return clients;
    }

    public static void main(String[] args) throws IOException {

        /**READ DATA from file using Scanner*/

        int clientnr, queuenr,tMaxSimulation, minArrivalTime,maxArrivalTime, minServiceTime, maxServiceTime;
        String temp1="";
        //File file = new File("in-1-test.txt");

        String fileName = args[0];
        String outString = args[1];

        File file = new File(fileName);



        Scanner sc = new Scanner(file);

        clientnr = sc.nextInt(); queuenr = sc.nextInt(); tMaxSimulation = sc.nextInt();
        temp1 = sc.nextLine(); temp1 = sc.nextLine();
        String forSplit[]  = temp1.split(",",2);
        minArrivalTime = Integer.parseInt(forSplit[0]);
        maxArrivalTime = Integer.parseInt(forSplit[1]);
        temp1 = sc.nextLine();
        forSplit = temp1.split(",",2);
        minServiceTime = Integer.parseInt(forSplit[0]);
        maxServiceTime = Integer.parseInt(forSplit[1]);

        /** creating the data structures which will be used for the implementation*/

        Client[] clients = new Client[clientnr];
        clients = generateClients(clientnr, minArrivalTime, maxArrivalTime, minServiceTime, maxServiceTime);
        Qu[] queues = new Qu[queuenr];
        WaitingQueue waitingQ = new WaitingQueue();

        /** INITIALIZATION of the objects according to the input data */

        for (int i=0; i<queuenr; ++i) {
            queues[i] = new Qu("closed",0);
        }

        ClientCopy[] clientcopies = new ClientCopy[clientnr];
        for (int i=0; i<clientnr; ++i) {
            clientcopies[i] = new ClientCopy(clients[i].ID, clients[i].serviceTime,clients[i].arrivalTime,0);
        }
        for (int i=0; i<clientnr; ++i) {
            waitingQ.setClientCopy(clientcopies[i]);
        }

        ServiceSystem serviceSystem = new ServiceSystem(waitingQ,0);
        for(int i=0; i<queuenr; ++i) {
            serviceSystem.setQ(queues[i]);

        }

        serviceSystem.time = tMaxSimulation;
        for (int i=0; i<clientnr; ++i)
            serviceSystem.clients.add(clients[i]);

        serviceSystem.outstring = outString;

        /** The call of the serviceSystem's run method is equal with "opening the shop" */

        serviceSystem.run();
        serviceSystem.interrupt();


    }

}


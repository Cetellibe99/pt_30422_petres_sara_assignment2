package Model;

import java.util.ArrayList;
import java.util.concurrent.*;
import java.util.List;
import java.util.*;

/**
 * @author Petres Sara 30422
 * This class represents the waiting queue
 * it is represented with the ConcurrentMap data structure which is
 * a concurrent thread safe data structure
 * the @removedList list is used for holding those clients
 * who are "leaving" the waiting queue i.e. those who arrived to the shop
 * before determining to which queue to go*/


public class WaitingQueue extends Thread {

    public ConcurrentMap <ClientCopy, Integer> waitingClients ;
    public List <ClientCopy> removedList;

    public WaitingQueue() {
        waitingClients= new ConcurrentHashMap();
        removedList = new ArrayList<>();
    }
    public void setClientCopy(ClientCopy c) {
        this.waitingClients.put(c,0);
    }

    public void printWQ() {

        Set set = waitingClients.entrySet();
        Iterator i = set.iterator();
        while(i.hasNext()) {
            Map.Entry me = (Map.Entry)i.next();
            ClientCopy c = (ClientCopy) me.getKey();
            c.printClientCopy();
            //System.out.println(me.getValue());
        }
    }

    /** its run method is responsible for decreasing the arrivalTime of the
     * auxiliar ClientCopy object in each second (until it decreases -> 0)
     */
    public void run() {

        Set set = waitingClients.entrySet();
        Iterator i = set.iterator();
        while(i.hasNext()) {
            Map.Entry me = (Map.Entry)i.next();
            ClientCopy c = (ClientCopy) me.getKey();
            if (c.arrivalTime == 0) {
                removedList.add(c);
                waitingClients.remove(me.getKey());
            }
            else
                c.arrivalTime = c.arrivalTime-1;
        }
    }

}

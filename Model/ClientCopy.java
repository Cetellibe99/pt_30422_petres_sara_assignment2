package Model;

/** @author Petres Sara 30422
 *
 * This class as an auxiliary Client class
 * it has an extra waitingTime parameter which is used for measurng the waiting time
 * the system operates on the ClientCopy objects and in the and associate them
 *with their corresponding Client object (by ID)
 *
 *Why they are used?
 *The arrival of a client is measured by decreasing its copy's arrivalTime parameter at each step, until it is 0
 *The service time of a client is measured by decreasing its copy's serviceTime which it spends in the queue
 *The waiting time of a client is measured by increasing its copy's waitingTime parameter
 *which is initially 0, at each second while it is in the queue
 */


public class ClientCopy {
    public int ID;
    public int serviceTime;
    public int arrivalTime;
    public int waitingTime;

    public ClientCopy(int ID, int serviceTime, int arrivalTime, int waitingTime) {
        this.ID=ID;
        this.serviceTime=serviceTime;
        this.arrivalTime = arrivalTime;
        this.waitingTime=waitingTime;
    }
    public int getID() {
        return ID;
    }
    public void setID(int iD) {
        ID = iD;
    }
    public int getArrivalTime() {
        return arrivalTime;
    }
    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
    public int getServiceTime() {
        return serviceTime;
    }
    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }
    public void printClientCopy() {

        System.out.println("ID ="+ID+"  arrTime="+arrivalTime+ "  serviceTime "+serviceTime+"  waitingtime= "+waitingTime+"\n");
    }


}

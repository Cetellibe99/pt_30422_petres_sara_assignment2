package Model;
import Model.Client;

import java.util.concurrent.*;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

/**
 * @author Petres Sara 30422
 * This is the implementation of the queue using the synchronized data structure
 * BlockingQueue for queues
 * Its run() method is responsible for removing the head of the queue
 * in case of service time finished
 *
 * Its @wTime parameter holds the total waitingTime of those clients who finished the shopping
 * Its @status parameter says if the queue is open or closed
 * */

public class Qu extends Thread {
    public BlockingQueue<Client> qu;
    public BlockingQueue<ClientCopy> quCopy;
    public String status;
    public int wTime;

    public Qu(String status, int wTime) {
        qu = new ArrayBlockingQueue(1024);
        quCopy = new ArrayBlockingQueue(1024);
        this.status = status;
        this.wTime = wTime;
    }

    public void setClient(Client c) {
        this.qu.add(c);
    }
    public BlockingQueue<Client> getClient( ) {
        return qu;
    }
    public void setClientCopy(ClientCopy c) {
        this.quCopy.add(c);
    }
    public void printQu() {
        for (int i=0; i<this.qu.size(); ++i) {

            System.out.println("the status is : " +status+ "\n");
        }
        try {
            Iterator <ClientCopy> itr = this.quCopy.iterator();
            while(itr.hasNext()) {
                itr.next().printClientCopy();
            }
        }
        catch(ConcurrentModificationException e) {
            System.out.println(e);
        }
    }

    public void run() {

        try { /**increase waitingtime of each client copy in each unit of time*/
            Iterator <ClientCopy> itrW = this.quCopy.iterator();
            while(itrW.hasNext() ) {
                ++itrW.next().waitingTime;
            }
        }
        catch(ConcurrentModificationException e) {
            System.out.println(e);
        }
        /**remove peek and save its waiting time*/
        if (quCopy.peek()!=null)
        {
            if (quCopy.peek().serviceTime == 1)
            {
                wTime = wTime + quCopy.peek().waitingTime;
                quCopy.remove();
            }
            else
                --quCopy.peek().serviceTime;
        }
    }
}

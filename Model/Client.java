package Model;

/** @author Petres Sara 30422*/

public class Client {
    public int ID;
    public int serviceTime;
    public int arrivalTime;
    public Client(int ID, int serviceTime, int arrivalTime) {
        this.setID(ID);
        this.setServiceTime(serviceTime);
        this.setArrivalTime(arrivalTime);
    }
    public int getID() {
        return ID;
    }
    public void setID(int iD) {
        ID = iD;
    }
    public int getArrivalTime() {
        return arrivalTime;
    }
    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
    public int getServiceTime() {
        return serviceTime;
    }
    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }
    public void printClientOut() {

        System.out.print("("+ID+","+arrivalTime+ ","+serviceTime+") ");
    }


}
